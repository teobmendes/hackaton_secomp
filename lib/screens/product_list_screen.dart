import 'package:appbev/screens/checkout.dart';
import 'package:appbev/models/product.dart';
import 'package:appbev/models/product_card.dart';
import 'package:flutter/material.dart';
import 'package:appbev/mocked/products.dart';



class ProductsCards extends StatefulWidget {
  const ProductsCards();

  @override
  _ProductsCardsState createState() => _ProductsCardsState();
}

class _ProductsCardsState extends State<ProductsCards> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Selecione os itens"),
        backgroundColor: Colors.orange,
      ),
      body: Scrollbar(
        child: ListView(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          children: [
            for (final product in products)
              Container(
                key: UniqueKey(),
                margin: const EdgeInsets.only(bottom: 8),
                child: SelectableProductItem(product: product),
              ),
            Container(height: 60,)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Checkout()),
            );
          },
          child: const Icon(Icons.shopping_cart),
      ),
    );
  }
}
