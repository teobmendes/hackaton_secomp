import 'package:flutter/material.dart';

class Sharing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: const Text("Compartilhe a loja"),
      ),
      body: Center(
        child: Container(
          width: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

            children: <Widget>[
              Container(),
              const Icon(
                Icons.share,
                size: 50,
              ),
              Column(
                children: [
                  const Text(
                      "Compartilhe o código desta loja e ganhe descontos de entrega!"),
                  Container(height: 8,),
                  Container(
                      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                      decoration: const BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(10.0))),
                      child: const Center(
                        child: Text(
                          "ASD56ASBJ",
                          style: TextStyle(color: Colors.white, fontSize: 22),
                          textAlign: TextAlign.center,
                        ),
                      )),
                  Container(height: 40,),
                  RaisedButton(
                    onPressed: () {},
                    color: Colors.amberAccent,
                    child: Text("Compartilhar"),
                  )
                ],

              ),

            ],
          ),
        ),
      ),
    );
  }
}
