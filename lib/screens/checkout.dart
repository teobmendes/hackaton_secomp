import 'dart:developer' as developer;

import 'package:appbev/models/product.dart';
import 'package:appbev/screens/request_delivery.dart';
import 'package:appbev/support/timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:appbev/mocked/products.dart';

// List<Product> _products = [
//   const Product(
//       assetName: 'assets/products/corona.png',
//       title: "Cerveja Corona 330ml",
//       expiration: "Vencimento em dois dias!",
//       price: "1,00",
//       pcolor: Colors.red),
//   const Product(
//       assetName: 'assets/products/guarana.png',
//       title: "Guaraná Antarctica 350ml",
//       expiration: "Vencimento em 4 dias!",
//       price: "1,50",
//       pcolor: Colors.orange),
// ];

List<Product> _products = [
  products[0],
  products[1],
];

List<Widget> convertProductsToWidgets() {
  final List<Widget> prodList = [];
  for (final prod in _products) {
    prodList.add(ListTile(
      title: Text(prod.title),
      subtitle: Text(
          "R\$${prod.price}     x${prod.title.contains("Cerveja") ? 3 : 2}"),
    ));
  }

  return prodList;
}

Route _createRoute() {
  return PageRouteBuilder(
    transitionDuration: const Duration(milliseconds: 500),
    pageBuilder: (context, animation, secondaryAnimation) => Delivery(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(2.0, 0.0);
      const end = Offset.zero;
      final curve = Curves.ease;

      final tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

class Checkout extends StatefulWidget {
  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  Icon currentIcon = const Icon(
    Icons.sentiment_satisfied,
    size: 70,
    color: Colors.amberAccent,
  );
  bool _isButtonDisabled = false;
  bool _payed = false;

  void payed() {
    setState(() {
      _payed = true;
      currentIcon = const Icon(
        Icons.sentiment_very_satisfied,
        size: 70,
        color: Colors.green,
      );
    });
  }

  void timerCallback() {
    setState(() {
      if (!_payed) {
        _isButtonDisabled = true;
        currentIcon = const Icon(
          Icons.sentiment_very_dissatisfied,
          size: 70,
          color: Colors.red,
        );
      }
    });
  }

  void waitToDeliveryCallback() {
    setState(() {
      Navigator.of(context).push(_createRoute());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Carrinho"),
        backgroundColor: Colors.orange,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow: [
                BoxShadow(offset: Offset(3, 5), color: Colors.black12)
              ]),
          child: ListView(
            padding: const EdgeInsets.all(10.0),
            children: [
              Container(
                height: 20,
              ),
              ...convertProductsToWidgets(),
              Container(
                height: 40,
              ),
              const Divider(
                thickness: 2,
              ),
              RichText(
                text: const TextSpan(
                  style: TextStyle(),
                  children: <TextSpan>[
                    TextSpan(text: '....'),
                    TextSpan(
                        text: "Total",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                            color: Colors.black)),
                    TextSpan(
                        text: "  R\$6,00",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: RaisedButton(
                  onPressed: _isButtonDisabled
                      ? null
                      : () {
                    payed();
                  },
                  color: Colors.green[200],
                  child: const Icon(Icons.monetization_on),
                ),
              ),
              Center(
                child: Column(
                  children: [
                    const Text("Finalize antes do tempo acabar!"),
                    Container(
                      height: 30,
                    ),
                    if (_payed)
                      Container()
                    else
                      OtpTimer(
                        timerEndedCallback: timerCallback,
                        timerMaxSeconds: 10,
                      ),
                    currentIcon,
                    if (_payed)
                      const Text("O produto é seu!")
                    else if (_isButtonDisabled)
                      const Text("O produto voltou para a fila")
                    else
                      Container(),
                    if (_payed)
                      Column(
                        children: [
                          Opacity(
                            opacity: 0,
                            child: OtpTimer(
                              timerEndedCallback: waitToDeliveryCallback,
                              timerMaxSeconds: 2,
                            ),
                          ),
                          const CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Colors.amberAccent),
                          )
                        ],
                      )
                    else
                      Container()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
