import 'package:appbev/models/store_card.dart';
import 'package:flutter/material.dart';

class StoresCards extends StatefulWidget {
  const StoresCards();

  @override
  _StoresCardsState createState() => _StoresCardsState();
}

class _StoresCardsState extends State<StoresCards> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Lojas com descontos"),
        backgroundColor: Colors.orange,
      ),
      body: Scrollbar(
        child: ListView(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          children: [
            for (final store in stores(context))
              Container(
                margin: const EdgeInsets.only(bottom: 8),
                child: StoreItem(store: store),
              ),
          ],
        ),
      ),
    );
  }
}
