import 'package:flutter/material.dart';

class Delivery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tipo de entrega"),
        backgroundColor: Colors.orange,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                const Text("Buscar pessoalmente", style: TextStyle(fontSize: 20, ),),
                Container(height: 10,),
                RawMaterialButton(
                  onPressed: () {},
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(15.0),
                  shape: const CircleBorder(),
                  child: const Icon(
                    Icons.directions_walk,
                    size: 100.0,
                  ),
                ),
              ],
            ),
            Column(
              children: [
                const Text("Solicitar entrega", style: TextStyle(fontSize: 20, ),),
                Container(height: 10,),
                RawMaterialButton(
                  onPressed: () {},
                  fillColor: Colors.white,
                  padding: const EdgeInsets.all(15.0),
                  shape: const CircleBorder(),
                  child: const Icon(
                    Icons.motorcycle,
                    size: 100.0,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
