import 'dart:async';

import 'package:flutter/material.dart';

class OtpTimer extends StatefulWidget {
  final Function() timerEndedCallback;
  final int timerMaxSeconds;
  const OtpTimer({this.timerEndedCallback, this.timerMaxSeconds});

  @override
  _OtpTimerState createState() => _OtpTimerState();
}

class _OtpTimerState extends State<OtpTimer> {
  final interval = const Duration(seconds: 1);

  Timer _timer;
  int currentSeconds = 0;

  String get timerText =>
      '${((widget.timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')}: ${((widget.timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  void startTimeout([int milliseconds]) {
    final duration = interval;
    _timer = Timer.periodic(duration, (timer) {
      setState(() {
        currentSeconds = timer.tick;
        if (timer.tick >= widget.timerMaxSeconds) {
          timer.cancel();
          widget.timerEndedCallback();
        }
      });
    });
  }

  @override
  void dispose() {
    if (_timer != null ) {
      _timer.cancel();
    }
    super.dispose();
  }

  @override
  void initState() {
    startTimeout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        const Icon(Icons.timer),
        const SizedBox(
          width: 5,
        ),
        Text(timerText)
      ],
    );
  }
}