library mocked_products;

import 'package:appbev/models/product.dart';
import 'package:flutter/material.dart';

List<Product> products = [
  const Product(
      assetName: 'assets/products/corona.png',
      title: "Cerveja Corona 330ml",
      expiration: "Vencimento em dois dias!",
      price: "1,00",
      pcolor: Colors.red,
      available: 5),
  const Product(
      assetName: 'assets/products/guarana.png',
      title: "Guaraná Antarctica 350ml",
      expiration: "Vencimento em 4 dias!",
      price: "1,50",
      pcolor: Colors.orange,
      available: 2),
  const Product(
      assetName: 'assets/products/h2o.png',
      title: "H2O Laranja 500ml",
      expiration: "Vencimento em 08/10/2020",
      price: "3,00",
      pcolor: Colors.green,
      available: 7),
  const Product(
      assetName: 'assets/products/skol.png',
      title: "Cerveja Skol 300ml",
      expiration: "Vencimento em 10/10/2020",
      price: "3,00",
      pcolor: Colors.green,
      available: 1),
];
