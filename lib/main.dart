import 'dart:developer' as developer;

import 'package:appbev/screens/store_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
    home: App(),
    debugShowCheckedModeBanner: false,
  ));
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  // Create the initilization Future outside of `build`:

  Widget buttonText(String text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: const TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 40.0,
        fontFamily: 'Roboto',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: StoresCards(),
    );
  }
}
