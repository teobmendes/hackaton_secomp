// Copyright 2020 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:appbev/models/store.dart';
import 'package:appbev/screens/product_list_screen.dart';
import 'package:appbev/screens/share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

List<Store> stores(BuildContext context) => [
  const Store(
    assetName: 'assets/stores/armazem.jpeg',
    title: "Armazém de Barão",
    description: "Loja de bebidas",
    location: "A 1km de você",
  ),
  const Store(
    assetName: 'assets/stores/bar.jpeg',
    title: "Star Clean",
    description: "Bar & lanchonete",
    location: "A 2,5km de você",
  ),
  const Store(
    assetName: 'assets/stores/mercado.jpeg',
    title: "Pague Tudo",
    description: "desc 1",
    location: "A 3km de você",
  ),
];

class StoreItem extends StatelessWidget {
  const StoreItem({Key key, @required this.store, this.shape})
      : assert(store != null),
        super(key: key);

  // This height will allow for all the Card's content to fit comfortably within the card.
  static const height = 360.0;
  final Store store;
  final ShapeBorder shape;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            SizedBox(
              height: height,
              child: Card(
                // This ensures that the Card's children are clipped correctly.
                clipBehavior: Clip.antiAlias,
                shape: shape,
                child: StoreContent(store: store),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key key,
    this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(4, 4, 4, 12),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(title, style: Theme.of(context).textTheme.subtitle1),
      ),
    );
  }
}

class StoreContent extends StatelessWidget {
  const StoreContent({Key key, @required this.store})
      : assert(store != null),
        super(key: key);

  final Store store;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final titleStyle = theme.textTheme.headline5.copyWith(color: Colors.white);
    final descriptionStyle = theme.textTheme.subtitle1;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 184,
          child: Stack(
            children: [
              Positioned.fill(
                // In order to have the ink splash appear above the image, you
                // must use Ink.image. This allows the image to be painted as
                // part of the Material and display ink effects above it. Using
                // a standard Image will obscure the ink splash.
                child: Ink.image(
                  image: AssetImage(
                    store.assetName,
                  ),
                  fit: BoxFit.cover,
                  child: Container(),
                ),
              ),
              Positioned(
                bottom: 16,
                left: 16,
                right: 16,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  alignment: Alignment.bottomLeft,
                  child: Chip(
                    avatar: const CircleAvatar(
                      child: Icon(Icons.store),
                    ),
                    // padding: const EdgeInsets.all(15.0),
                    label: Text(store.title, style: TextStyle(color: Colors.black, fontWeight: FontWeight.w800),),
                  ),
                ),
              ),
            ],
          ),
        ),
        // Description and share/explore buttons.
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: DefaultTextStyle(
            softWrap: false,
            overflow: TextOverflow.ellipsis,
            style: descriptionStyle,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // This array contains the three line description on each card
                // demo.
                Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    store.description,
                    style: descriptionStyle.copyWith(color: Colors.black54),
                  ),
                ),
                Chip(
                  avatar: const CircleAvatar(
                    child: Icon(Icons.directions_walk),
                  ),
                  backgroundColor: Colors.lightGreen[100],
                  label: Text(store.location),
                ),
              ],
            ),
          ),
        ),
        // share, explore buttons
        ButtonBar(
          alignment: MainAxisAlignment.start,
          children: [
            FlatButton(
              textColor: Colors.amber.shade500,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Sharing()),
                );
              },
              child: const Text("share", semanticsLabel: "asd"),
            ),
            RaisedButton(
              color: Colors.amber.shade500,
              textColor: Colors.white,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProductsCards()),
                );
              },
              child: const Text("explorar", semanticsLabel: "nada"),
            ),
          ],
        ),
      ],
    );
  }
}

