import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';


class Store {
  const Store({
    @required this.assetName,
    @required this.title,
    @required this.description,
    @required this.location,
  })  : assert(assetName != null),
        assert(title != null),
        assert(description != null),
        assert(location != null);

  final String assetName;
  final String title;
  final String description;
  final String location;
}