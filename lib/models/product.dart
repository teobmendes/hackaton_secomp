import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Product {
  const Product({
    @required this.assetName,
    @required this.title,
    @required this.expiration,
    @required this.price,
    @required this.pcolor,
    @required this.available,
  })  : assert(assetName != null),
        assert(title != null),
        assert(expiration != null),
        assert(price != null),
        assert(pcolor != null),
        assert(available != null);

  final String assetName;
  final String title;
  final String expiration;
  final String price;
  final MaterialColor pcolor;
  final int available;
}