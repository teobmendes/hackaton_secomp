# AppBev

Aplicativo com proposta inovadora de centralização da venda de produtos AB InBEV próximos ao vencimento.
Aqui o usuário deve esperar que todo e qualquer item estará abaixo do preço de mercado tradicional!


## Instalação
Para instalar o app, simplesmente execute:

```shell
$ flutter pub get
$ flutter run
```

## Screenshots

#### Lista de lojas com produtos com desconto
<img src="screenshots/lojas.jpg" alt="scale=0.5" style="zoom:50%;" />
  
   
   
#### Seleção de produtos na loja desejada
<img src="screenshots/selecao.jpg" alt="scale=0.5" style="zoom:50%;" />
  
  
  
#### Tempo limite para pagamento
O tempo limite para pagamento é necessário para resolver um problema logístico introduzido pelo app.
Se um usuário do app colocar um item no carrinho, ele ficará indisponível para todos os outros usuários 
até que o pedido tenha algum tipo de conclusão.  
Para evitar que o produto fique muito tempo fora da lista o usuário terá um tempo limitado para completar o pagamento do pedido.  

<img src="screenshots/checkout.jpg" alt="scale=0.5" style="zoom:50%;" />
  
  
  
#### Exemplo de pedido perdido por tempo esgotado
<img src="screenshots/perdido.jpg" alt="scale=0.5" style="zoom:50%;" />
  
  
  
#### Exemplo de pedido adquirido com sucesso
<img src="screenshots/adquirido.jpg" alt="scale=0.5" style="zoom:50%;" />
  
  
  
#### Seleção do tipo de entrega do produto
<img src="screenshots/entrega.jpg" alt="scale=0.5" style="zoom:50%;" />

